import consola from 'consola'
import db from './db'
import i18n from './i18n'
import utils from './utils'

module.exports = async function (moduleOptions) {
  const options = Object.assign({}, this.options.nu, moduleOptions || {})
  const nuxt = this

  await db.init(options, nuxt)

  await i18n.init(options, nuxt)

  await utils.init(options, nuxt)
  // process.exit()
}

// module.exports.meta = require('./package.json')
