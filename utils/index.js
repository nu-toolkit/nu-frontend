import consola from 'consola'
import nodePath from 'path'

const init = async function ({ utils }, nuxt) {
  if (utils === false) {
    consola.info('Utils disabled (config)')
    return
  }

  nuxt.addPlugin({
    src: nodePath.resolve(__dirname, './plugin.js')
  })

  consola.success('Utils plugin attached')
}

export default {
  init
}
