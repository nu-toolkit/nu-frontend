const get = function (obj, app) {
  // console.log(this.$i18n.locale)
  if (typeof obj !== 'object') return undefined
  const { locale, defaultLocale } = this.$i18n || app.i18n || {}
  if (obj[locale]) return obj[locale]
  else if (obj[defaultLocale]) return obj[defaultLocale]
  else return undefined
}

export default (ctx, inject) => {
  inject('get', get)
}
