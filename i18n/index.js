import consola from 'consola'
import getDbName from '../db/getDbName'
import slug from 'slug'
const slugify = (x) => (slug(x)).toLowerCase()

const init = async function ({ i18n, db }, nuxt) {
  if (!i18n) {
    consola.info('I18n disabled (config)')
    return
  }

  const { langs, translateRoutes, seo } = i18n
  if (!langs || langs.length === 0) {
    consola.info('I18n disabled (missing langs)')
    return
  }

  const langsSimple = langs.map(lang => (lang.code || lang))

  const pages = {}

  if (translateRoutes && translateRoutes.length > 0 && nuxt.dbs && nuxt.dbOpts) {
    const { prefix, postfix } = nuxt.dbOpts
    const [pageDb] = nuxt.dbs.filter(db => db.name === getDbName({ prefix, postfix, name: 'page' }))

    if (pageDb) {
      const menuDoc = await pageDb.get('menu').catch(() => undefined)

      const getSlugs = ({ page, prefix, postfix, menu } = {}) => {
        const slugs = {}
        langsSimple.forEach(lang => {
          if (menuDoc[menu] && menuDoc[menu][lang]) {
            slugs[lang] = (prefix
              ? '/' + prefix
              : '') +
            '/' + slugify(menuDoc[menu][lang]) +
            (postfix
              ? '/' + postfix
              : '')
          }
        })
        return slugs
      }
      // console.log(pages)

      translateRoutes.forEach(({ page, menu, prefix, postfix }) => {
        if (!menu) menu = page
        if (page && menu && menuDoc[menu]) pages[page] = getSlugs({ page, menu, prefix, postfix })
        else consola.warn('Wrong i18n config!')
      })
      // console.log(pages)
      // process.exit()
    }
  }

  nuxt.addModule(['nuxt-i18n', {
    locales: langs,
    defaultLocale: langsSimple[0],
    seo,
    parsePages: pages === {}, // Disable acorn parsing
    pages: pages
  }], true)

  consola.success('I18n module attached')
}

export default {
  init
}
