const PouchDB = require('pouchdb-core')
const adapterHttp = require('pouchdb-adapter-http')
const mapReduce = require('pouchdb-mapreduce')
const find = require('pouchdb-find')

module.exports = {
  PouchDB,
  adapterHttp,
  mapReduce,
  find
}
