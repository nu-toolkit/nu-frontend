import consola from 'consola'
import nodePath from 'path'
import express from 'express'
import merge from 'lodash/merge'
import ora from 'ora'
import getDbName from './getDbName'
import pouch from './pouch'
import expressPouch from './expressPouch'

const app = express()

const defaultOptions = {
  enabled: true,
  path: './tmp',
  prefix: 'nu'
}

const getOptions = (db) => {
  const { development = {}, production = {}, ...mainOptions } = merge(
    defaultOptions,
    db
  )

  const enviroments = { development, production }

  const all = merge(mainOptions, enviroments[process.env.NODE_ENV] || {})

  return all
}

const syncDbs = async ({ source, dbs, sync }) => {
  const RemotePouch = pouch({ prefix: source + '/' })
  const spinner = ora('Database pulling').start()
  const promises = dbs.map(
    localDb =>
      new Promise((resolve, reject) => {
        const { name } = localDb
        const remoteDb = new RemotePouch(name.replace('-dev', ''))

        // console.log(localDb.name, remoteDb.name)

        localDb.replicate
          .from(remoteDb)
          .on('complete', (info) => {
            if (process.env.npm_lifecycle_script !== 'nuxt build') {
              localDb.sync(remoteDb, {
                live: true,
                retry: true,
                pull: true,
                push: false
              })
            }

            // spinner.stop()
            return resolve(info)
          })
          .on('error', (error) => {
            // spinner.fail(`Failed to pull collection ${name}`)
            throw error
          })
      })
  )

  await Promise.all(promises)

  spinner.succeed('Database pulled and syncing')
}

const init = async function ({ db }, nuxt) {
  const opts = getOptions(db)

  const {
    enabled,
    source,
    path,
    sync,
    collections,
    prefix,
    postfix
  } = opts

  if (enabled === false || db === {}) {
    consola.info('Database disabled (config)')
    return
  }

  if (!collections || collections.length === 0) {
    consola.info('Database disabled (no collections configured)')
    return
  }

  const Pouch = pouch()

  app.use('/db', await expressPouch({ path, Pouch }))

  const dbs = collections.map(
    name => new Pouch(getDbName({ prefix, postfix, name }))
  )
  // consola.info('Database collections:', collections.join(', '))

  if (sync === false) {
    consola.info('Database sync disabled (config)')
  }

  if (!source) {
    consola.info('Database sync disabled (no source configured)')
  } else {
    await syncDbs({ source, dbs, sync })
  }

  nuxt.addServerMiddleware({
    path: '/',
    handler: app
  })

  nuxt.addPlugin({
    src: nodePath.resolve(__dirname, './plugin.js'),
    options: opts
  })

  consola.success('Database plugin attached')

  nuxt.dbs = dbs
  nuxt.dbOpts = opts
}

export default {
  getOptions,
  init
}
