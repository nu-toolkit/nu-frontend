const proxy = require('http-proxy-middleware')

export default (source) => proxy({
  target: source,
  changeOrigin: true, // for vhosted sites, changes host header to match to target's host
  logLevel: 'debug',
  pathRewrite: { '^/db': '' },
  onProxyReq: (proxyReq, req, res) => {
    // add custom header to request
    proxyReq.setHeader('Authorization', 'Basic NjkxZGNiNGItMGYwZS00MGQ5LWE4N2ItZTA0YWIyYTBmZmIxLWJsdWVtaXg6NjdmMzYxN2Q0YzYzMjlmZWFmOGE2OWYzMTUwMmJjZmNhNzMwZmZlMzE0M2YwNDcxYzA4MDdjMTQ5NTEwZmQ0Ng==')
    // or log the req
  }
  // onProxyRes: (proxyRes, req, res) => {
  //   // add custom header to request
  //   proxyRes.headers['Access-Control-Allow-Origin'] = '*'
  //   proxyRes.headers['Access-Control-Allow-Methods'] = 'GET,PUT,POST,DELETE,PATCH,OPTIONS'
  //   // or log the req
  // }
})
