const pouch = require('pouchdb-core')
pouch.plugin(require('pouchdb-adapter-http'))
pouch.plugin(require('pouchdb-adapter-memory'))
pouch.plugin(require('pouchdb-mapreduce'))
pouch.plugin(require('pouchdb-replication'))
pouch.plugin(require('pouchdb-find'))

module.exports = function ({ adapter, prefix } = {}) {
  return pouch.defaults({
    adapter,
    prefix
  })
}
