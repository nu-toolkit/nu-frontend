import PouchDB from 'pouchdb-core'
import adapterHttp from 'pouchdb-adapter-http'
import mapReduce from 'pouchdb-mapreduce'
import find from 'pouchdb-find'

import getDbName from 'nu-frontend/db/getDbName'

PouchDB.plugin(adapterHttp)
PouchDB.plugin(mapReduce)
PouchDB.plugin(find)

let host
if (process.client) {
  const port = window.location.port ? ':' + window.location.port : ''
  host = `${window.location.protocol}//${window.location.hostname}${port}`
} else {
  host = 'http://0.0.0.0:3000'
}

const initCollections = (configDb) => {
  const { collections, prefix, postfix } = configDb

  const dbHost = `${host}/db/`

  const dbs = {}

  return (name) => {
    if (!name) throw Object({ error: true, msg: 'Missing collection name' })

    const idx = collections.indexOf(name)
    if (idx === -1) throw Object({ error: true, msg: 'Missing collection' })

    if (!dbs[name]) dbs[name] = new PouchDB(dbHost + getDbName({ name, prefix, postfix }))

    return dbs[name]
  }
}

const dbOptions = <%= JSON.stringify(options, null, 2) %>

export default (ctx, inject) => {
  inject('db', initCollections(dbOptions))
}
