export default ({ prefix, postfix, name }) => {
  if (prefix && postfix) return `${prefix}-${name}-${postfix}-collection`
  else if (prefix && !postfix) return `${prefix}-${name}-collection`
  else if (!prefix && postfix) return `${name}-${postfix}-collection`
  else return `${name}-collection`
}
